<?php
session_start();
require_once("../includes/connection.php");
require_once("../template/layoutFunctions.php");
require_once("../includes/siteFunctions.php");
require_once("../includes/login_check.php");
require_once("../includes/admin_check.php");

if ($_SESSION['redirect'] != true){
	renderHeader('Edit User');
	
	// Make sure the user ID passed into the form is an integer
	$userIDtoEdit =  intval($_GET['uidedit']);
	
	// SQL query to get the correct user
	$user = mysql_query("
		SELECT *
		FROM users
		WHERE uid = ". $userIDtoEdit ."
		");
	
	//Render form to edit the user
	$user = mysql_fetch_array($user);

	// If the form was submitted
	if($_POST['userName']){
		
		//Check data and get possible error messages
		$errorMessages = $errorMessages = verifyUserData(
			$_POST['userName'],
			$_POST['email'],
			$_POST['password'],
			$_POST['passwordVerify'],
			$_POST['uid']
		);

		// IF No errors, UPDATE THE USER!
		if(empty($errorMessages)){

			$_POST['password'] = mysql_real_escape_string($_POST['password']);
			
			// update the users table for the current user
			$sqlQuery = "
				UPDATE users
				SET username = '". $_POST['userName'] ."',";
			// if password was set, let's update the password	    
			if($_POST['password']){
				$sqlQuery .= "
						password = '". crypt($_POST['password'],'tacos') ."',";
			}
			$sqlQuery .= "
						email = '". $_POST['email'] ."',
						role = '". $_POST['role']."'
				WHERE uid = ". intval($_POST['uid']);
			$userInsert = mysql_query($sqlQuery);

			// Update session information on succes (mysql_query returns true)
			if($userInsert){
				header("location: /admin/viewusers.php");
			}else{
				$errorMessages[] = 'Error with inserting entered data. Please retry.';
			}
		}
	}


	?>
	<div class="row">
		<div class="large-12 columns">
			<h2>Edit User
			<form id="editUser" name="edituser" method="post" action="">
				<input type="hidden" name="uid" value="<?php print $user['uid']; ?>"/>
			
				<label>Username</label>
		    <input type="text" name="userName" size="40" value="<?php print $user['username']; ?>"/>
				
				<label>Permissions</label>

				<select name="role">
					<option value="general" <?php if($user['role'] == 'general'){print 'selected';} ?>>General</option>
			  	<option value="admin" <?php if($user['role'] == 'admin'){print 'selected';} ?>>Administrator</option>
				</select>

				<label>Email</label>
		    <input type="text" name="email" size="40" value="<?php print $user['email']; ?>"/>
			
				<label>Password:<br/></label> 
		    <input id="userPassword" type="password" name="password" size="30" placeholder="Enter a new password, or ignore to leave alone."/>
		    <input id="userPassword" type="password" name="passwordVerify" size="30" placeholder="Verify password a second time here."/>

				<input type="submit" value="Save Changes"/>
			</form>
		</div>
	</div>

<?php
	renderFooter();
}