<?php
session_start();
require_once($_SERVER['DOCUMENT_ROOT']."/includes/connection.php");
require_once($_SERVER['DOCUMENT_ROOT']."/template/layoutFunctions.php");
require_once($_SERVER['DOCUMENT_ROOT']."/includes/siteFunctions.php");
require_once($_SERVER['DOCUMENT_ROOT']."/includes/login_check.php");
require_once($_SERVER['DOCUMENT_ROOT']."/includes/admin_check.php");

if ($_SESSION['redirect'] != true){
	

	// If the form was submitted
	if($_POST['userName']){
		
		$errorMessages = verifyUserData(
			$_POST['userName'],
			$_POST['email'],
			$_POST['password'],
			$_POST['passwordVerify']
		);

		// No errors, UPDATE THE USER!
		if(empty($errorMessages)){
			$_POST['password'] = mysql_real_escape_string($_POST['password']);

			// Query to create the new user
			$sqlQuery = "
			INSERT INTO users (
				username, uid, email, role, password
				)
			VALUES (
				'". $_POST['userName'] ."', NULL , '". $_POST['email'] ."',
				'". $_POST['role']."', '". crypt($_POST['password'],'tacos') ."'
			)";

			// Create user and return true/false for success or failure
			$userInsert = mysql_query($sqlQuery);

			// Update session information on succes (mysql_query returns true)
			if($userInsert){
				$_SESSION['message'][] = 'A new account has been created!';
				header("location: /admin/viewusers.php");
			}else{
				$errorMessages[] = 'Error with inserting entered data. Please retry.';
			}

		}
	}

	renderHeader('Add A User');

	?>
<div class="row">
  <div class="large-12 columns">
	<form name="adduser" method="post" action="">
		<h2 style="margin-top:0;">Add a User</h2>
    
    <label>Username:</label> 
    <input type="text" name="userName" size="40" value="<?php echo $_GET['username'] ?>"/>
	
		<label>Permissions: </label>
    <select name="role">
	  	<option value="general">General</option>
	  	<option value="admin">Administrator</option>
		</select>

		<label>Password:</label> 
    <input type="password" name="password" size="40" value="" />
    <input type="password" name="passwordVerify" size="40" value="" />

		<label>Email: </label>
	  <input type="text" name="email" size="40" value=""/>
	
		<input type="submit" value="Add User"/>
	</div>
</div>
	<?php renderFooter();
}
