<?php
session_start();
require_once("../template/layoutFunctions.php");
require_once("../includes/login_check.php");
require_once("../includes/admin_check.php");
require_once("../includes/connection.php");

if ($_SESSION['redirect'] != true){
		
	// Displays users with a role of admin
	if($_GET['present'] == 'admin'){
		renderHeader('View Users - Admin');
		$users = mysql_query("
			SELECT *
			FROM users
			WHERE role = 'admin'
			ORDER BY username ASC
		");
	}
	// Display users with a regular role (IE: Not admin)
	else if($_GET['present'] == 'users'){
		renderHeader('View Users - Regular User');
		$users = mysql_query("
			SELECT *
			FROM users
			WHERE role = 'general'
			ORDER BY username ASC
		");
	}
	// Display ALL users, with admins first
	else{
		renderHeader('View Users');
		$users = mysql_query("
			SELECT *
			FROM users
			ORDER BY role ASC, username ASC
		");
		}
	?>
	
	<div style="width: 500px; margin: 20px auto 0 auto; padding:0;">
    <h3 style="margin-top:0;">User Filters</h3>
	<ul>
		<li><a href="viewusers.php?present=admin">Admins</a></li>
		<li><a href="viewusers.php?present=users">Regular Users</a></li>
		<li><a href="viewusers.php?">All users</a></li>
	</ul>
	</div>
    
    	<table cellpadding="5" style="width: 500px; margin: 0 auto;">
		<thead>
			<tr>
				<th align="left">Username</th>
				<th align="left">Role</th>
				<th align="left">Email</th>
				<th align="left"></th>
			</tr>
		</thead>
		<tbody>
			<?php
				while($row = mysql_fetch_array($users)){
					print'<tr>';
						print '<td>'.$row["username"].'</td>';
						print '<td>'.$row["role"].'</td>';
						print '<td>'.$row["email"].'</td>';
						print '<td><a href="editUser.php?uidedit='.$row["uid"].'">Edit</a></td>';

					print'</tr>';
				}
			?>
		</tbody>
	
	<?php	
		
	/* end content */
	
	renderFooter();
}


?>