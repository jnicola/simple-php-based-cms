<?php

/**
 *  This renders the header of the page
 */

function renderHeader($pageTitle){
	// Enter to customize your sites $base_url if there is an issue.
	$base_url = '/';

	// You will print the header here, and the $pageTitle variable is available for naming the page!
	print'<!doctype html>';
	print'<head>';
	print'<title>'.$pageTitle.' | Custom CMS</title>';
	print'<meta name="viewport" content="width=device-width, initial-scale=1.0" />';
	/* Scripts and styles */
	print'<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>';
	print'<script language="javascript" src="'.$base_url.'includes/foundation.min.js"></script>';
	print'<link rel="stylesheet" type="text/css" href="'.$base_url.'styles/foundation1.css" />';
	print'<link rel="stylesheet" type="text/css" href="'.$base_url.'styles/foundation2.css" />';

	print'</head>';
	
	
	print '<body>';
	print '<div class="row">
				  <div class="large-12 columns">
				    <h1>CCAE PHP -- Animal Shelter Example</h1>
				  </div>
				</div>';
	
	
	// Wrapper for top bar section
	print'
	<div class="contain-to-grid sticky" data-options="sticky_on: large">
	<nav class="top-bar" data-topbar>
				<section class="top-bar-section">';
	// Right side admin options
	if (
    (isset($_SESSION['role']) && $_SESSION['role'] == 'admin')
    || (isset($_SESSION['role']) && $_SESSION['role'] == 'administrator')) {
		print '
				<ul class="right">
					<li><a href="'. $base_url .'admin/viewusers.php">View Users</a></li>
					<li><a href="'. $base_url .'admin/adduser.php">Add User</a></li>
				</ul>
		';
	}
	
	if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"]) {
		print '
			  <ul class="left">
			  	<li><a href="'. $base_url .'index.php">Home</a></li>
			    <li><a href="'. $base_url .'editProfile.php">Edit Profile</a></li>
					<li><a href="'. $base_url .'logout.php">Logout</a></li>
			  </ul>';
	}
	else{
		print'
			  <ul class="left">
			    <li><a href="'. $base_url .'">Home</a></li>
					<li><a href="'. $base_url .'">Login</a></li>
					<li><a href="'. $base_url .'register.php">Register For Account</a></li>
			  </ul>
		';
	}
	print'</nav>
	</div>';

	/**
	 * Print any session messages!
	 */
	if(isset($_SESSION['message']) && count($_SESSION['message']) > 0){
		print '<div class="row">
			<div class="large-12 columns">';
		foreach($_SESSION['message'] as $message){
			print '<div data-alert class="alert-box warning round">'. $message .'</p></div>';
		}
		print'</div></div>';
		unset($_SESSION['message']);
	}
}

/**
 *  This function renders the footer of the document
 */

function renderFooter() {
	//you will put the footer code here. No variables are currently passed to this.
	print '</div>';
	print '</body>';
	// ALWAYS be sure to terminate MySQL connection
}
