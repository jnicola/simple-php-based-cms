<?php
session_start();
require_once("includes/connection.php");
require_once("includes/siteFunctions.php");
require_once("template/layoutFunctions.php");


if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) {

	// If the form was submitted
	if (isset($_POST['userName'])) {
		
		$errorMessages = verifyUserData(
			$_POST['userName'],
			$_POST['email'],
			$_POST['password'],
			$_POST['passwordVerify']
		);

		// No errors, UPDATE THE USER!
		if(empty($errorMessages)){
			$_POST['password'] = mysql_real_escape_string($_POST['password']);

			// Query to create the new user
			$sqlQuery = "
			INSERT INTO users (
				username, uid, email, role, password
				)
			VALUES (
				'". $_POST['userName'] ."', NULL , '". $_POST['email'] ."',
				'general', '". crypt($_POST['password'],'tacos') ."'
			)";

			// Create user and return true/false for success or failure
			$userInsert = mysql_query($sqlQuery);

			// Update session information on succes (mysql_query returns true)
			if($userInsert){
				$_SESSION['message'][] = 'Your account has been created!';
				header("location: /index.php");
			}else{
				$errorMessages[] = 'Error with inserting entered data. Please retry.';
			}

		}
	}
	// If a user wasn't insert above, either due to error or no form post
	if(!$userInsert){
		renderHeader('Register for an account');

		// If error message exists, and has messages, display them!
		if(isset($errorMessages) && count($errorMessages) > 0){
			print '
			<div class="row">
				<div class="large-12 columns">
					<div data-alert class="alert-box warning round">';
					foreach($errorMessages as $errorMessage){
						print $errorMessage . '<br/>';
					}
			print '
					</div>
				</div>
			</div>';
		}

	?>
		

	<div class="row">
		<div class="large-12 columns">
			<form id="editProfile" name="register" method="post" action="register.php">
				<h2>Register for an Account</h2>
				<label>Username</label>
				<input type="text" name="userName" size="30" value=""/>
				<label>Password:</label> 
	      <input type="password" name="password" size="30"  placeholder="Enter your password here." />
				<input id="password" type="password" name="passwordVerify" placeholder="Enter Password again here to verify it is correct."/>
				<label>Email:</label>
				<input type="text" name="email" size="30"  value=""/>
				<input type="submit" value="Register"/>
			</form>
		</div>
	</div>
	<?php
				
		renderFooter();
	}
}else{
	$_SESSION['message'][] = 'You area already logged in, and therefore cannot and should not create a new account. Please logout before attempting to create a new account!';
	header("location: /index.php");
}

?>
