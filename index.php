<?php	

session_start();

require_once("includes/siteFunctions.php");
require_once("includes/connection.php");
require_once("template/layoutFunctions.php");

$_SESSION['times'][] = 'been here...';

if (!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] != true) { // Display login page if not logged in
	require_once("includes/login.php");
	exit;
}
else { // User logged in, render site!
  renderHeader('Home');
  // @TODO: Add content here
  renderFooter();
}
