<?php
session_start();
require_once("template/layoutFunctions.php");
require_once("includes/siteFunctions.php");
require_once("includes/login_check.php");
require_once("includes/connection.php");

if ($_SESSION['redirect'] != true){
	
	// Get the current user
	$userLookup = mysql_query("
		SELECT *
		FROM users
		WHERE uid = ".$_SESSION['uid']."
		");

	//Get the user data from the MySQL query.
	$currentUser = mysql_fetch_array($userLookup);

	// If the form was submitted
	if($_POST['userName']){



		//initate the error message array
		$errorMessages = $errorMessages = verifyUserData(
			$_POST['userName'],
			$_POST['email'],
			$_POST['password'],
			$_POST['passwordVerify']
		);

		// No errors, UPDATE THE USER!
		if(empty($errorMessages)){

			$_POST['password'] = mysql_real_escape_string($_POST['password']);
			// update the users table for the current user
			$sqlQuery = "
				UPDATE users
				SET username = '". $_POST['userName'] ."',";
			// if password was set, let's update the password	    
			if($_POST['password']){
				$sqlQuery .= "
						password = '". crypt($_POST['password'],'tacos') ."',";
			}
			$sqlQuery .= "
						email = '". $_POST['email'] ."'
				WHERE uid = ". $_SESSION['uid'];
			$userInsert = mysql_query($sqlQuery);

			// Update session information on succes (mysql_query returns true)
			if($userInsert){
				$_SESSION['successEditProfile'] = true;
				header("location: /editProfile.php");
			}else{
				$errorMessages[] = 'Error with inserting entered data. Please retry.';
			}
		}
	}

	// Start rendering the page
	renderHeader('Edit Profile');

	// If error message exists, and has messages, display them!
	if(isset($errorMessages) && count($errorMessages) > 0){
		print '
		<div class="row">
			<div class="large-12 columns">
				<div data-alert class="alert-box warning round">';
				foreach($errorMessages as $errorMessage){
					print $errorMessage . '<br/>';
				}
		print '
				</div>
			</div>
		</div>';
	}

	// If form submit succesfully occured previous page load
	if(empty($_POST) && isset($_SESSION['successEditProfile']) && $_SESSION['successEditProfile']){
		print '
				<div class="row">
					<div class="large-12 columns">
						<div data-alert class="alert-box round">
						 Your user information has been updated!
						</div>
					</div>
				</div>';
		$_SESSION['successEditProfile'] = FALSE;
	}
?>

<div class="row">
	<div class="large-12 columns">
		<form id="editProfile" name="edituser" method="post" action="editProfile.php">
			<h2>Edit Your Profile</h2>
			<label>User Name:</label>
			<input type="text" name="userName" size="30" value="<?php print $currentUser['username']; ?>"/>

			<label>Email Address:</label>
			<input type="text" name="email" size="30" value="<?php print $currentUser['email']; ?>"/>

			<label>Change Password:</label> 
			<input id="password" type="password" name="password" placeholder="Only type here if you wish to change your password"/>
			<input id="password" type="password" name="passwordVerify" placeholder="Verify your password if you wish to change it"/>

			<input type="submit" value="Save Changes"/>
		</form>
	</div>
</div>

<?php
	}
	renderFooter();
