<?php
include_once('connection.php');
$skipForm = FALSE;

/**
 *  If data was submitted via $_POST
 */
if (isset($_POST["username"]) && $_POST["username"] != ''){
  $validates = FALSE;
  // @TODO: SANITIZE INPUTS!
	$username = mysqli_real_escape_string($connection, $_POST["username"]);
	$password = crypt(mysqli_real_escape_string($connection, $_POST["password"]), $settings['salt']);

	$query = "
		SELECT *
		FROM users
		WHERE username = '{$username}'
		AND password = '{$password}'
	";
  $result = mysqli_fetch_all(mysqli_query($connection, $query));

	if (count($result) == 1) {
    $_SESSION["loggedin"] = true;
    $_SESSION["redirect"] = false;
    // @TODO: Add user information to session.
    $skipForm = true;
    header('location: /index.php');
  }
	else {
    $_SESSION['redirect'] = false;
    $_SESSION['message'][] = 'Login or password incorrect! Try again!';
    $skipForm = false;
  }




}						

/**
 *  Display login form ONLY if the user has not logged in properly above.
 */

if ($skipForm != true) {
		$loginForm ='
<div id="siteLogin" style="width:250px; margin: 30px auto 0 auto;">
	<h2>Log in to the Site</h2>
	<form id="loginForm" action="" method="post">
		<table>
			<tr>
				<td>
					<label>Username:</label>
				</td>   
				<td>    
					<input class="username" name="username" type="text" value="Username" onfocus="if(!this._haschanged){this.value=\'\'};this._haschanged=true;">
				</td>
			</tr>
			<tr>
				<td>
					<label>Password:</label>
				</td>
				<td>
					<input class="password" name="password" type="password" value="Password" onfocus="if(!this._haschanged){this.value=\'\'};this._haschanged=true;">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" value="submit">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<a href="register.php">Register for an account</a>
				</td>
			</tr>
        </table>
    </form>
	<div id="clr"></div>
	</div>
	';
	renderHeader('Login');
	print $loginForm;
}