<?php
if (!isset($_SESSION)) {session_start();}

$_SESSION["page_name"] = $_SERVER["SCRIPT_NAME"];

if(!$_SESSION["loggedin"] || $_SESSION["loggedin"] != true) {
    header('location: /index.php');
		$_SESSION['redirect'] = true;
    }
else {
     $_SESSION['redirect'] = false;
     }
	 