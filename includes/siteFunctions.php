<?php
if (!isset($_SESSION) && !headers_sent()){ // starts session only if one isn't started already
	session_start();
}

/**
 * Send an email out when there is a new user
 */

function newUser($userName){

	$adminCheck = mysql_query("
			SELECT *
			FROM users
			WHERE permission = 'Administrator' OR
			permission = 'Admin'
		");
			
	// determining who to send the email to
	$first = true;
	$mailTo = '';
	while($row = mysql_fetch_array($adminCheck)){
		if ($first == true){
			$mailTo = $row['email'];
			}
		else{
			$mailTo = $mailTo . ', ' . $row['email'];
			}
		$first = false;
	}
	
	// preparing a new email
	$subject = 'A new user has registered on the site!';
	$message = 'Please check the site, a new user with the user name <b>' . $userName . '</b> has signed up and shall require verification in order to proceed.';
	$message = wordwrap($message, 70);
	
	// To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: do-not-reply@jessenicla.com' . "\r\n" .
		'Reply-To: do-not-reply@jessenicola.com' . "\r\n";

	if(mail($mailTo, $subject, $message, $headers)){}else{
		print '<h1>ERROR!</h1>';
		die();
		}
}

/**
 * This function checks if a user has permissions to access a particular region
 */

function permission($area){
	if ($_SESSION['uid'] != null){ // user must be logged in or don't run!
		$permCheck = mysql_query("
			SELECT ". $area ."
			FROM users
			WHERE uid = ".$_SESSION['uid']."
		");
		$result = mysql_fetch_array($permCheck);
		// If the users result is TRUE for has access. True is stored as 1
		if ($result[0] == 1){
			return true; // has access
		}
		else{ // does not have access
			return false; 
		}
	}else{// user not logged in. Does not have access.
		return false;
	}
}



/**
 *  Custom function for verifying emails
 *  @param: String email addres.
 *  @return: Boolean of whether an email is correct or not.
 */

function checkEmail($email) {
	// Uses PHP built in filter_var functionality and a regular expression
	// Verifying there is an @ and a . in the email address
	return
		filter_var($email, FILTER_VALIDATE_EMAIL)
		&& preg_match('/@.+\./', $email); 
}


/**
 * Check validity of submitted user data before creating/editing user
 */

function verifyUserData($username, $email, $password, $passwordVerify, $uid = NULL){
	// If there is a UID, have to verify username against UID.
	if($uid){
		// Get the username for current user ID
		$takenUserNames = mysql_query("
			SELECT username
			FROM users
			WHERE uid != ". intval($uid) ."
		");

	}elseif(isset($_SESSION['uid'])){ // Get current UID
		// get the username for current user ID
		$takenUserNames = mysql_query("
			SELECT username
			FROM users
			WHERE uid != ".$_SESSION['uid']."
		");
	}else{
		// Get --ALL-- usernames
		$takenUserNames = mysql_query("
			SELECT username
			FROM users
			WHERE 1
		");	
	}

	//Get the taken username user data from the MySQL query.
	while($dbUserName = mysql_fetch_array($takenUserNames)){
		$allUserNames[] = $dbUserName[0]; 
	}

	//initate the error message array
	$errorMessages = array();

	// Check that USERNAME is alphanumeric without spaces
	if(!ctype_alpha($username)){
		$errorMessages[] = 'Username must be alphabet letters only without spaces!';
	}

	// Check if username is already taken. Should allow current username
	if(in_array($username, $allUserNames)){
		$errorMessages[] = 'Username is already taken. Please choose a different name.';
	}

	// check format of email address
	if(!checkEmail($email) ){
		$errorMessages[] = 'Email entered is invalid.';
	}

	// Make sure password is 4 characters or longer
	if(!empty($password) && strlen($password) < 4){
		$errorMessages[] = 'Password must be at least 4 characters';
	}

	// If password provided, verify it matches
	if(!empty($password) && $password != $passwordVerify){
		$errorMessages[] = 'Passwords entered do not match!';
	}

	return $errorMessages;
}



/**
 *  --------------------- PLEASE IGNORE THIS CODE!!! ---------------------
 */


/**
 *	send out administrative warning
 */ 

function adminWarn($subject,$message){
	if (mysql_close() == true){
		require_once($_SERVER['DOCUMENT_ROOT']."/operationsApp/includes/connection.php");

		$adminCheck = mysql_query("
				SELECT *
				FROM users
				WHERE permission = 'Administrator' OR
				permission = 'Admin'
				");
		$date = date('m/d/Y');
		
		// add entry to database
				$addEntry = mysql_query("
					INSERT INTO `errors` (
						`type` ,
						`issue` ,
						`date`
						)
						VALUES (
						'".$subject."',
						'".$subject."',
						'".$date."'
						);
				");
		
		
	// determining who to send the email to
		
		$first = true;
		$mailTo = '';
		while($row = mysql_fetch_array($adminCheck)){
			if ($first == true){
				$mailTo = $row['email'];
				}
			else{
				$mailTo = $mailTo . ', ' . $row['email'];
				}
			$first = false;
		}
		
		$mailTo = 'jesse@jessenicola.com';
		
	// preparing a new email
		$subject = $subject;
		$message = $message;
		$message = wordwrap($message, 70);
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: do-not-reply@go.skibowl.com' . "\r\n" .
			'Reply-To: do-not-reply@go.skibowl.com' . "\r\n";

		if(mail($mailTo, $subject, $message, $headers)){}else{
			print '<h1>ERROR!</h1>';
			die;
		}
	}
}
