<?php

$settings = [
  'host',
  'dbName',
  'userName',
  'password',
  'salt',
];

if (@include_once('settings.php')) {
  print 'Settings.php exists, so you may have incorrect values. Adjst manually!';
  die();
}

if(isset($_POST['install'])){ // Form was submitted to install file, handle submission below
    print'<h1>Attempting to install:</h1>
    <ul>';
    $errorEncountered = false;

    // first, sterilize code.
    $_POST['host'] = preg_replace("/[^A-Za-z0-9.]/", "", $_POST['host']); // Only allow A# and periods for host
    $_POST['dbName'] = preg_replace("/[^A-Za-z0-9]/", "", $_POST['dbName']);   //only allow A# for db name
    $_POST['userName'] = preg_replace("/[^A-Za-z0-9]/", "", $_POST['userName']);   //only allow A# for username
    $_POST['password'] = preg_replace("/[^A-Za-z0-9~!@#$%^&*-+=|\/?]/", "", $_POST['password']); // limits password special characters

    // test if mysql connections are good and if database is empty
    $connectionMain = mysqli_connect($_POST['host'], $_POST['userName'], $_POST['password']);
    if (!$connectionMain){
        $error .= 'Issue Connecting as Main Database User<br/>';
        $errorEncountered = true;
    }else{
        print'<li>Primary DB Connection Works</li>';
        mysqli_close($connectionMain);
    }

    if (!$errorEncountered) {
        // checks if database exists
      $db_connection = mysqli_connect($_POST['host'], $_POST['userName'], $_POST['password']);
        $db_select = mysqli_select_db($db_connection, $_POST['dbName']);
        if(!$db_select) {
            if(mysqli_query($db_connection, "SELECT * FROM `users` WHERE 1") !== false){
                $error .= 'Database already Exists<br/>';
                $errorEncountered = true;
            }
        }
        else {
          print'<li>Database found!</li>';

          // Create users table.
          if (!$errorEncountered && mysqli_query($db_connection,"CREATE TABLE IF NOT EXISTS users
            (
            username text NOT NULL,
            uid int(11) NOT NULL AUTO_INCREMENT,
            email varchar(128) NOT NULL,
            role text NOT NULL,
            password varchar(32) NOT NULL,
            PRIMARY KEY (uid)
            );
            ")) {
            print '<li>User Table Created or Exists already!</li>';
          }
          else {
              print 'ERROR! Unable to create user table!';
            $errorEncountered = TRUE;
          }

          $sa_un = $_POST['saun'];
          $sa_email = $_POST['saemail'];
          $crypted_su_password = crypt($_POST['supassword'], $_POST['salt']);

          if (!$errorEncountered && mysqli_query($db_connection, "
            INSERT INTO users
                (username, uid, email, role, password)
            VALUES
                ('{$sa_un}', 1, '{$sa_email}', 'admin', '{$crypted_su_password}');
        ")) {
            print '<li>Superadmin created!</li>';
          }
          else{
            print 'ERROR! Unable to create user table!';
            $errorEncountered = TRUE;
          }
        }
        mysqli_close($db_connection);
    }

    if (!$errorEncountered) {
        // @TODO: Change settings to use an array instead of individual values
        $fh = fopen('settings.php', "w");
        fwrite($fh, "<?php\n");
        fwrite($fh, '$settings = [' . "\n");
        foreach ($settings as $setting_key) {
            $setting = '  "' . $setting_key . '" => "' . $_POST[$setting_key] . '"' . ",\n";
          fwrite($fh, $setting);
        }
        fwrite($fh, "];\n");
        fclose($fh);

        print'<li>Settings file written! Welcome to your new site!</li><ul>';
        print '<p><a href="/index.php">Load the home page</a></p>';
    }
    else {
        print '<p>Unable to complete install. Please try again with adjusted values.</p>';
    }
}

if (!isset($_POST['install']) || $errorEncountered) {
    if (isset($errorEncountered)) {
        print '<hr/>';
    }
    ?>
    <h1>New Site Install</h1>
    <form action="install.php" method="post">

        <div style="border:1px solid #000">
            <h2>Database Settings</h2>
            <div>
                <input type="hidden" name="install" value="true"/>
                <label>Host: (leave blank to use localhost)</label>
                <input type="text" name="host"/>
            </div>

            <div>
                <label>Database Name</label>
                <input type="text" name="dbName"/>
            </div>

            <div>
                <label>Database User Name</label>
                <input type="text" name="userName"/>
            </div>

            <div>
                <label>Database User Password:</label>
                <input type="password" name="password"/>
                <p>Only the following special characters are allowed: <em>~!@#$%^&*-+=|\/?]/</em></p>
            </div>
        </div>
        <div style="border:1px solid #000">
            <h2>Site Settings</h2>
            <div>
                <label>Sitewide SALT:</label>
                <input type="text" name="salt"/>
                <p>A string text known as a "salt" used for encrypting password so no plain text passwords are stored.</p>
            </div>
        </div>
        <div style="border:1px solid #000">
            <h2>Super Admin Settings</h2>
            <div>
                <label>Super Admin Username:</label>
                <input name="saun"/>
                <p>Super Admin Username (You will login with those to do anything you need.</p>
            </div>

            <div>
                <label>Super Admin email:</label>
                <input name="saemail"/>
                <p>Super Admin Email. For requesting a new password.</p>
            </div>


            <div>
                <label>Super Admin Password:</label>
                <input type="password" name="supassword"/>
                <p>Super Admin Password!</p>
            </div>
        </div>

        <div>
            <input type="submit" value="Install"/>
        </div>
    </form>
<?php
}
