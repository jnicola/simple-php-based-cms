<?php
session_start();

$_SESSION["page_name"] = $_SERVER["SCRIPT_NAME"];
if($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'administrator') {
    $_SESSION['redirect'] = false;
    }
else {
		header('location: /index.php');
    $_SESSION['redirect'] = true;
    }
	 