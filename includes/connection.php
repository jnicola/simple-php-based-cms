<?php
if (!include_once('settings.php')) {
  header('location: includes/install.php');
  die;
}

if (isset($settings['host'])) {
  if ($settings['host'] == '') {
    $host = 'localhost';
  }
	$connection = mysqli_connect($host, $settings['userName'], $settings['password']);
	
	if(!$connection) {
		 die("1. Database connection failed: " . mysqli_error($connection));
		 exit;
	}else{
		// 2. Select a database to use
		if($db_select = mysqli_select_db($connection, $settings['dbName']) == false) {
			 die(" 2. Database selection failed: " . mysqli_error($connection));
			 exit;
		}
	}
}

 else { // Connection variable not yet written to file, site needs to be installed
 	print '<p>An error occured trying to connect to the DB, check the values in settings.php';
 	die;
 }
